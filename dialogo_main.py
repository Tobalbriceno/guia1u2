#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class DialogoMain():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./window.pagina")

        self.dialogo1 = builder.get_object("dialogo_delete")
        self.dialogo1.set_default_size(400, 300)
        self.dialogo1.set_title("¿Està seguro que desea eliminar lo guardado?")

        cancelar = builder.get_object("boton_dialogo_cancelar")
        cancelar.connect("clicked", self.on_boton_dialogo_cancelar_clicked)
        self.dialogo1.show_all()

    def on_boton_dialogo_cancelar_clicked(self, boton=None):
        self.dialogo1.destroy()

