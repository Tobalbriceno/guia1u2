#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dialogo_main import DialogoMain

class ConstructorWindow():

    def __init__(self):
        
        builder = Gtk.Builder()
        builder.add_from_file("./window.pagina")
    
        window = builder.get_object("create")
        window.connect("destroy", Gtk.main_quit)
        window.resize(800, 600)
        window.show_all()

        botonA = builder.get_object("boton_aceptar")
        botonA.connect("clicked", self.on_boton_aceptar_click)

        botonR = builder.get_object("boton_reset")
        botonR.connect("clicked", self.on_boton_reset_click)


        botonD = builder.get_object("boton_clean")
        botonD.connect("clicked", self.on_boton_clean_click)


        self.texto = builder.get_object("ingreso_texto")

        self.numero = builder.get_object("ingreso_numero")

        self.label_ing = builder.get_object("label_ing")

        self.label_num = builder.get_object("label_num")


    def on_boton_aceptar_click(self, boton=None):
        texto = self.label_ing.get_text()
        numero = self.label_num.get_text()
        self.texto.set_text(texto)
        self.numero.set_text(numero)


    def on_boton_reset_click(self, boton=None):
        texto = self.texto.get_text()
        self.label_ing.set_text(texto)
        numero = self.numero.get_text()
        self.label_num.set_text(numero)


    def on_boton_clean_click(self, boton=None):
        print("clik aka")
        ventana = DialogoMain()
        response = dialogo.dlg.run()

        if response == Gtk.ResponseType.CANCEL:
            ventana.dialogo.destroy() 

        else:
            self.label_ing.set_text("")
            self.label_num.set_text("")



if __name__ == "__main__":
    ConstructorWindow()
    Gtk.main()
